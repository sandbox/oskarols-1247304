<?php

class SaploAPI {

  public static $debug = TRUE;
  private $endpoint = 'http://api.saplo.com/rpc/json';
  //private $endpoint = 'http://foobarfoobarfoobar.se';
  private $token;
  private $jsonRequest;
  private $jsonBatch;
  private $authCredentials;
  private $retried;

  function __construct($api_key, $secret_key, $endpoint = '') {
    if(!empty($endpoint))
    $this->endpoint = $endpoint;
    	
    $this->authCredentials = array("api_key" => $api_key, "secret_key" => $secret_key);
    $this->token = variable_get('saplo_session_token', NULL);

    // Catches both if the drup. var is null and an empty session arg.
    // So basic procedure is to try to get a session var from variable_get,
    // and even if it's either not set or invalid, it'll sort itself out.
    if ( ! $this->token) {
      $this->accessToken($this->authCredentials);
    }

    $this->text = new Text($this);
    $this->collection = new Collection($this);
    $this->group = new Group($this);
    $this->account = new Account($this);
  }

  function accessToken($params) {
    $response = $this->doRequest("auth.accessToken", $params);
    $object = $this->parseResponse($response, $params);
    // Returns undefined index if try to set access_token and it isn't available
    // E.g. error..
    if (array_key_exists('access_token', $object))
    $this->token = $object['access_token'];
    variable_set('saplo_session_token', $this->token);
    watchdog(SAPLO_WATCHDOG, 'Reset API Session token to: ' . $this->token);
    return $object;
  }

  /**
   * Provide your own raw json string.
   * @param String $json
   * @return String JSON formatted string
   */
  public function custom_request($json) {
    return $this->post($json);
  }

  function doRequest($method, $params) {

    if(isset($params['request_id'])) {
      $request_id = $params['request_id'];
      unset($params['request_id']);
    } else
    $request_id = 0;
    	
    if(isset($params['trim']))
    unset($params['trim']);

    $request = array(
    	"method" => $method,
    	"params" => $params,
    	"id" => $request_id,
    	"jsonrpc" => "2.0"
	);

	$requestJson = json_encode($request);
	$this->jsonRequest = $requestJson;

	SaploAPI::debug("JSON-Request: ".$requestJson);

	return $this->post($requestJson);
  }

  function post($requestJson) {
    $postUrl = $this->endpoint."?access_token=".$this->token;
    
    //set the url, number of POST vars, POST data
    $responseobj = drupal_http_request($postUrl, array(), 'POST', $requestJson);
    
    // @TODO: Catch connectivity issues with the API
    // None JSON output
    // Timeout

    return $responseobj->data;
  }

  function parseResponse($response, $params) {
    $parsed = json_decode($response, true);

    SaploAPI::debug("JSON-Response: ".$response);
    if (isset($parsed['result'])) {
      return $parsed['result'];
    }
    elseif (isset($parsed['error'])) {
      // If the session has expired, automatically fetch a new one and re-do the request.
      if (key_exists('code', $parsed['error']) AND $parsed['error']['code'] === 595 AND $this->retried <= 3) {
        return $this->retryRequest();
      }
      else {
        return $parsed['error'];
        #throw new SaploException($parsed['error']['msg'], $parsed['error']['code'], $this->jsonRequest);
      }
      // Because the API is down, we simulate a new errorcode.
    }
    else {
      return array(
				'code' => 0,
				'msg' => 'Unable to connect to the API'
				);
    }
  }

  private function trim($param, $array) {
    return $array[$param];
  }

  private function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  private function getEndpoint() {
    return $this->endpoint;
  }

  static function getAccessToken() {
    return $this->token;
  }

  private function retryRequest() {
    $this->retried = isset($this->retried) ? $this->retried++ : 1;

    # TODO@ Won't this be overwriten on subsequent retries?
    $tempRequestStore = $this->jsonRequest;

    $this->accessToken($this->authCredentials);
    $response = $this->post($tempRequestStore);

    // TODO@ not knowing whether the trim param was set in args would in effect
    // render a retry able to send different response than the original call....
    // E.g. not good. Remove trim entirely.
    $parsed_response = $this->parseResponse($response, null);

    // Reset the counter, since request was successful.
    if (! key_exists('code', $parsed_response)) {
      $this->retried = 0;
    }
    return $parsed_response;
  }

  private function debug($str) {
    #if(SaploAPI::$debug)
    #  echo $str."\n\n";
  }
}

class Account {

  function __construct($api) {
    $this->api = $api;
  }

  function get($params = array()) {
    $response = $this->api->doRequest("account.get", $params);
    return $this->api->parseResponse($response, $params);
  }
}

class Collection {

  function __construct($api) {
    $this->api = $api;
  }

  function create($params) {
    $response = $this->api->doRequest("collection.create",$params);
    return $this->api->parseResponse($response, $params);
  }

  function get($params) {
    $response = $this->api->doRequest("collection.get", $params);
    return $this->api->parseResponse($response, $params);
  }

  function update($params) {
    $response = $this->api->doRequest("collection.update",$params);
    return $this->api->parseResponse($response, $params);
  }

  function delete($params) {
    $response = $this->api->doRequest("collection.delete", $params);
    return $this->api->parseResponse($response, $params);
  }

  //Can be called using method name 'list'
  function lst($params = array()) {
    $response = $this->api->doRequest("collection.list", $params);
    return $this->api->parseResponse($response, $params);
  }

  function reset($params) {
    $response = $this->api->doRequest("collection.reset", $params);
    return $this->api->parseResponse($response, $params);
  }

  public function __call($func, $args) {
    switch ($func) {
      case 'list':
        $params = isset($args[0]) ? $args[0]: array();
        $request_id = isset($args[1]) ? $args[1]: 0;
        $trim = isset($args[2]) ? $args[2]: TRUE;
        return $this->lst($params);
        break;
    }
  }
}


class Text {

  function __construct($api) {
    $this->api = $api;
  }

  function create($params) {
    $response = $this->api->doRequest("text.create", $params);
    return $this->api->parseResponse($response, $params);
  }

  function update($params) {
    $response = $this->api->doRequest("text.update", $params);
    return $this->api->parseResponse($response, $params);
  }

  function get($params) {
    $response = $this->api->doRequest("text.get", $params);
    return $this->api->parseResponse($response, $params);
  }

  function delete($params = 0) {
    $response = $this->api->doRequest("text.delete", $params);
    return $this->api->parseResponse($response, $params);
  }

  function tags($params) {
    $response = $this->api->doRequest("text.tags", $params);
    return $this->api->parseResponse($response, $params);
  }

  function related_texts($params) {
    $response = $this->api->doRequest("text.relatedTexts", $params);
    return $this->api->parseResponse($response, $params);
  }

  function related_groups($params) {
    $response = $this->api->doRequest("text.relatedGroups", $params);
    return $this->api->parseResponse($response, $params);
  }
}

class Group {

  function __construct($api) {
    $this->api = $api;
  }

  function create($params) {
    $response = $this->api->doRequest("group.create",$params);
    return $this->api->parseResponse($response, $params);
  }

  function get($params) {
    $response = $this->api->doRequest("group.get",$params);
    return $this->api->parseResponse($response, $params);
  }

  function update($params) {
    $response = $this->api->doRequest("group.update",$params);
    return $this->api->parseResponse($response, $params);
  }

  function reset($params) {
    $response = $this->api->doRequest("group.reset",$params);
    return $this->api->parseResponse($response, $params);
  }

  function delete($params) {
    $response = $this->api->doRequest("group.delete",$params);
    return $this->api->parseResponse($response, $params);
  }

  //Can also be called using method name 'list'
  function lst($params = array()) {
    $response = $this->api->doRequest("group.list", $params);
    return $this->api->parseResponse($response, $params);
  }

  function add_text($params) {
    $response = $this->api->doRequest("group.addText", $params);
    return $this->api->parseResponse($response);
  }

  function delete_text($params) {
    $response = $this->api->doRequest("group.deleteText", $params);
    return $this->api->parseResponse($response);
  }

  function list_texts($params) {
    $response = $this->api->doRequest("group.listTexts", $params);
    return $this->api->parseResponse($response, $params);
  }

  function related_texts($params) {
    $response = $this->api->doRequest("group.relatedTexts", $params);
    return $this->api->parseResponse($response, $params);
  }

  function related_groups($params) {
    $response = $this->api->doRequest("group.relatedGroups", $params);
    return $this->api->parseResponse($response, $params);
  }

  public function __call($func, $args) {
    switch ($func) {
      case 'list':
        $params = isset($args[0]) ? $args[0]: array();
        $request_id = isset($args[1]) ? $args[1]: 0;
        $trim = isset($args[2]) ? $args[2]: TRUE;
        return $this->lst($params, $trim);
        break;
    }
  }

}