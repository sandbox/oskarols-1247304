
<?php if ( ! empty($relevant_articles)) { ?>
<h4>Relevant articles <small style="font-size:70%">(by <a href="http://www.saplo.com/api/">Saplo API</a>)</small></h4>
<ul id="saplo_relevance">
  <?php foreach($relevant_articles as $article) { ?>
	<li><a href='<?php echo $article['url'] ?>'> <?php echo $article['title'] ?></a>
      <?php if ($show_relevance) { ?>
		<span>(<?php echo $article['relevance'] ?>)</span>
      <?php } ?>
	</li>
  <?php } ?>
</ul>
<?php } ?>