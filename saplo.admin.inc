<?php

/**
 * Implementation of hook_perm().
 */
function saplo_perm() {
  return array(
    'administer saplo' => array(
      'title' => t('Administer Saplo module'),
      'description' => t('Allow user to configure the Saplo module'),
    ), 
    'access saplo' => array(
      'title' => t('Access Saplo'),
      'description' => t("Allow user to see and press the 'Add Saplo Metadata'-button on viewing nodes."),
    )
  );
}


function saplo_admin() {
  $form = array();

  $form['api_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enter your Saplo API Keys'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['api_keys']['intro'] = array(
	'#value' => "<p>Please enter the API Keys that you received from <a href='http://saplo.com/signup'>signing up</a> to the Saplo API.</p>",
  );
  $form['api_keys']['saplo_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Saplo API key'),
    '#default_value' => variable_get('saplo_api_key', ""),
    '#size' => 40,
    '#maxlength' => 32,
    '#description' => t("The API key given on signup to the Saplo API."),
    '#required' => TRUE,
  );
  $form['api_keys']['saplo_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Saplo Secret key'),
    '#default_value' => variable_get('saplo_secret_key', ""),
    '#size' => 40,
    '#maxlength' => 32,
    '#description' => t("The Secret API key given on signup to the Saplo API."),
    '#required' => TRUE,
  );
  if ( ! module_exists('locale')) {
    $languages = array('en' => 'English', 'sv' => 'Swedish');
    $form['saplo_default_language'] = array(
      '#type' => 'select',
      '#title' => 'Choose default site language',
      '#default_value' => variable_get('saplo_default_language', ""),
      '#options' => $languages,
      '#description' => 'Choose default language. If your site only contains a single language, then choose that one.',
      '#required' => TRUE,
    );
  }
  $form['corpus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['corpus']['information'] = array(
    '#value' => '<p>If left empty, the collections will be created automatically and the id:s filled in below.</p>'
  );
  $form['corpus']['saplo_english_corpus_id'] = array(
    '#type' => 'textfield',
    '#title' => t('English corpus API ID'),
    '#default_value' => variable_get('saplo_english_corpus_id', ""),
    '#size' => 8,
    '#maxlength' => 8,
    '#description' => t("ID for an english corpus you've created."),
    '#required' => FALSE,
  );
  $form['corpus']['saplo_swedish_corpus_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Swedish corpus API ID'),
    '#default_value' => variable_get('saplo_swedish_corpus_id', ""),
    '#size' => 8,
    '#maxlength' => 8,
    '#description' => t("ID for an swedish corpus you've created."),
    '#required' => FALSE,
  );

  // Adds additional things to the form, submit button etc.
  return system_settings_form($form);
}

// Saplo Admin Forms
function saplo_admin_tags() {
  $form = array();

  $form['tagging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node tagging'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['tagging']['saplo_tag_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Enable tag extraction for these node types',
    '#description' => 'Please select the content type for which you would like to enable tag extraction.',
    '#default_value' => variable_get('saplo_tag_nodetypes', array()),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#multiple' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Adds additional things to the form, submit button etc.
  return system_settings_form($form);
}

// Saplo Admin Forms
function saplo_admin_relevance() {
  $form = array();

  $form['relevance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activated for node types'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['relevance']['saplo_relevance_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Enable relevance fetching for these node types',
    '#description' => 'Please select the content type for which you would like to enable relevance fetching.',
    '#options' => array_map('check_plain', node_get_types('names')),
    '#default_value' => variable_get('saplo_relevance_nodetypes', array()),
    '#multiple' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['threshold'] = array(
    '#type' => 'fieldset',
    '#title' => 'Relevance threshold',
    '#description' => 'To describe relevance (similarity of texts) between two nodes the Saplo API uses a decimal number between 0.00 and 1.00, where 1.00 is an exact duplicate of a text. Change these numbers if you want to manually tweak the relevance values.',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );


  $form['threshold']['saplo_relevance_max'] = array(
    '#type' => 'textfield',
    '#title' => 'Max threshold',
    // TODO@ reformat below
    '#description' => 'Setting this to a value of 1 will show nodes with duplicated content.',
    '#default_value' => variable_get('saplo_relevance_max', 0.99),
    '#size' => 5,
  );

  $form['threshold']['saplo_relevance_min'] = array(
    '#type' => 'textfield',
    '#title' => 'Minimum threshold',
    '#default_value' => variable_get('saplo_relevance_min', 0.50),
    '#size' => 5,
  );

  // Adds additional things to the form, submit button etc.
  return system_settings_form($form);
}

function saplo_admin_tags_validate($form, &$form_state) {
  $activated_node_types = array_filter($form_state['values']['saplo_tag_nodetypes']);
  
  $vid = variable_get('saplo_tags_vocab', NULL);
  
  if($vid) {
    db_query("DELETE FROM {vocabulary_node_types} WHERE vid = %d", $vid);
    foreach($activated_node_types as $type) {
      db_query("INSERT INTO {vocabulary_node_types} (vid, type) VALUES (%d, '%s')", $vid, $type);
    }
  }
  else {
    drupal_set_message(t('Unable to automatically activate node types for the vocabulary, to enable tag editing you will have to enable the node types manually using the taxonomy module.'), 'error');
  }
  
}

/**
 * Form validation handler.
 */
function saplo_admin_validate($form, &$form_state) {
  // TODO@ Running the validation function forces an accessToken call, which resets the saplo_session_token using variable_set
  // This in turn resets the cache, which can be a problem on big sites. Consider having some other way of authenticating.

  // Run validation if both fields aren't empty (default validation will cover error msg:es if they are)
  if ( $form_state['values']['saplo_api_key'] !== '' AND $form_state['values']['saplo_secret_key'] !== '' ){

    $api_key =		$form_state['values']['saplo_api_key'];
    $secret_key =	$form_state['values']['saplo_secret_key'];

    // Does explicit accessToken-call to get a response, which is used to check key validity
    $apiclient = new SaploAPI($api_key, $secret_key);
    $response = $apiclient->accessToken(array('api_key' => $api_key, 'secret_key' => $secret_key));

    // Note: if the API-keys were invalid in any way, then the response (from getSession) will be an array
    // containing an error msg.
    $invalid_keys = key_exists('code', $response);

    if ($invalid_keys) {
      form_set_error('api_keys', t('Invalid API Keys. Please verify that the keys are the same you received from Saplo.'));
    }
    else {
      // Check if the corpus ID's are set, if not then create corpae automatically.
      if($form_state['values']['saplo_english_corpus_id'] == '') {
        $en_coll = array(
    	  'name' => 'drupal_en',
    	  'description' => 'Automatically generated corpus from Drupal.',
    	  'language' => 'en'
		);
        $response = $apiclient->collection->create($en_coll);
        if( ! array_key_exists('code', $response)) {
          $form_state['values']['saplo_english_corpus_id'] = $response['collection_id'];
        }
        elseif(array_key_exists('code', $response) && $response['code'] === 1203) {
          drupal_set_message(t("Maximum number of collections exceeded."), "error");
          return;
        }
        else {
          drupal_set_message(t("Unable to create English collection"), "error");
        }
      }
       
      if($form_state['values']['saplo_swedish_corpus_id'] == '') {
        $sv_coll = array(
    		'name' => 'drupal_sv',
    		'description' => 'Automatically generated corpus from Drupal.',
    		'language' => 'sv'
		);
		$response = $apiclient->collection->create($sv_coll);

		if( ! key_exists('code', $response)) {
		  $form_state['values']['saplo_swedish_corpus_id'] = $response['collection_id'];
		}
		else {
		  drupal_set_message(t("Unable to create Swedish collection"), "error");
		  watchdog(SAPLO_WATCHDOG, 'Error creating Swedish collection');
		}

      }     
      if( ! saplo_vocabulary_already_exists('Saplo Tags') ){
        saplo_create_new_vocabulary();
      }
    }
  }
}