<?php


/**
 * Implements hook_theme()
 */
function saplo_theme($existing, $type, $theme, $path) {
  return array(
	'saplo_relevance' => array(
    	'arguments' => array('relevant_articles' => NULL, 'order' => 'DESC', 'show_relevance' => FALSE),
    	'template' => 'saplo-relevance',
  ),
  );
}

/**
 * Implements template_preprocess()
 */
function template_preprocess_saplo_relevance(&$variables) {
  if($variables['order'] === 'ASC') {
    $variables['relevant_articles'] = array_reverse($variables['relevant_articles']);
  }
}


/**
 * Theme function to show a single node.
 */
function saplo_show_relevance(&$node) {
  $node->content['saplo_relevance'] = array(
	'#value' => theme('saplo_relevance', saplo_relevance_get($node->nid, 5), 'DESC', TRUE),
	'#weight' => 10,
  );
}